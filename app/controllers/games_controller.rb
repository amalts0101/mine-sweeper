class GamesController < ApplicationController
  before_action :get_game, only: [:index, :new]
  def index
    @game = Game.new
  end

  def new
    @board = Game.new
  end

  def create
    @board = GameGeneratorService.new(params).generate_game
    respond_to do |format|
      format.js {render layout: false}
    end
  end

  private

  def get_game
    @game = Game.new
  end

end
