function startTimer(){
  var sec = 0;
  resetTimer();
  document.getElementById("lost").innerHTML='';
  $('#remainingCells').val('0');
  $('#gameOver').val('false');
  function pad ( val ) { return val > 9 ? val : "0" + val; }
  setInterval( function(){
      document.getElementById("seconds").innerHTML=pad(++sec%60);
      document.getElementById("minutes").innerHTML=pad(parseInt(sec/60,10));
  }, 1000);

}
function resetTimer(){
  const interval_id = window.setInterval(function(){}, Number.MAX_SAFE_INTEGER);

  // Clear any timeout/interval up to that id
  for (let i = 1; i < interval_id; i++) {
    window.clearInterval(i);
  }
}

function revealNeighbours(){
// TODO
}

function revealAllMines(cells){
  for(var i = 0; i < cells.length; i++){
    var cell = cells[i];
    img = cell.getElementsByTagName('img')[0];
    if (img === undefined) { continue; }
    img_val = img.getAttribute('value');
    if(img_val == 'x' && img.getAttribute('src') != "images/mine_blown.png"){
      cell.innerHTML = "<img src='images/mines.jpg' alt='' border=3 height=30 width=30></img>"
    }
  }
}

function setTotalCells(cells){
  var total = [];
  for(var i = 0; i < cells.length; i++){
    var cell = cells[i];
    img = cell.getElementsByTagName('img')[0];
    img_val = img.getAttribute('value');
    if (img_val != 'x'){
      total.push(img_val)
    }
  }
  $('#totalCells').val(total.length);
}

function checkWinStatus(cells, img_val){
  var counter = $('#remainingCells').val();
  counter++;
  $('#remainingCells').val(counter);
  var remainingCells = parseInt($('#remainingCells').val());
  var totalCells = parseInt($('#totalCells').val());
  var gameOver = $('#gameOver').val();
  debugger;
  if (totalCells-remainingCells == 0 && img_val != 'x' && gameOver != 'true'){
    showWin();
  }
  else{
    return true;
  }
}

function showWin(){
  alert('Congrats ! You won the game !')
}
