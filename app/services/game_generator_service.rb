class GameGeneratorService

  def initialize(params)
    @params = params
  end

  def generate_game
    height, width, mines = get_game_constraints
    board = Array.new(height) { Array.new(width, 0) }

    mines.times do
      x = rand(height)
      y = rand(width)

      redo if board[x][y] == 'x'

      board[x][y] = 'x'

      [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1], [x-1, y+1], [x-1, y-1], [x+1, y+1], [x+1, y-1]].each do |x, y|
        next if x < 0 || x >= height
        next if y < 0 || y >= width
        next if board[x][y] == 'x'

        board[x][y] += 1
      end
    end
    game = Game.create(board: board, height: height, width: width, mines: mines)
    return board
  end

  def get_game_constraints
    case @params[:difficulty]
    when 'beginner'
      height, width, mines = 9, 9, 10
    when 'intermediate'
      height, width, mines = 16, 16, 40
    when 'expert'
      height, width, mines = 16, 30, 99
    when 'custom'
      height, width, mines = @params[:game][:height].to_i, @params[:game][:width].to_i, @params[:game][:mines].to_i
    end
    [height, width, mines]
  end
end

# 9 x 9 - 10

# 16 x 16  - 40

# 16 x 30 - 99
