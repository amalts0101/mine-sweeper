# Minesweeper
Simple minesweeper game.
<table cellspacing="0" cellpadding="2" id="controls" class="dialog" style="display: table;">
        <tbody>
          <tr class="dialog-title">
          <td>Controls</td>
        </tr>
        <tr>
          <td style="padding: 6px;"><strong>Desktop</strong></td>
          <td style="padding: 6px;">
            <ul>
              <li><b>Left-click</b> an empty square to reveal it.</li>
              <li><b>Right-click</b> (or <b>Ctrl+click</b>) an empty square to flag it.</li>
            </ul>
          </td>
        </tr>
      </tbody>
</table>

# Follow these steps

- rails db:create
- rails assets:precompile 
