class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.text :board
      t.string :height
      t.string :width
      t.string :mines
      t.timestamps
    end
  end
end
